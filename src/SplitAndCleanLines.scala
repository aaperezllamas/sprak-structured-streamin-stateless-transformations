import org.apache.arrow.vector.types.pojo.ArrowType.Binary
import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.types.BinaryType

import scala.io.Source

/**
  * A Spark Structured Streaming application to transform text lines
  */
object SplitAndCleanLines {

  // TODO set your user name here
  val user: String = "aalvarez"

  val InputTopic = "streaming.books"

  private val logger = Logger.getLogger(getClass.getName)

  private val spark = SparkSession.builder()
    .appName(s"SplitAndCleanLines - $user")
    .master("yarn")
    .getOrCreate()

  // this is needed to use symbols as column names
  import spark.implicits._


  /**
    * Reads the file specified as the params and returns its content as a list of lines
    * @param filePath
    * @return
    */
  def getFileLines(filePath: String): List[String] = {
    val source = Source.fromFile(filePath)
    try {
      source.getLines().toList
    }
    finally {
      source.close()
    }
  }

  /**
    * Returns a streaming dataframe from the specified topic
    * @param kafkaTopic
    * @return
    */
  def getInputTableFromTopic(kafkaTopic: String): DataFrame = {
    spark.readStream
      .format("kafka")
      .option("subscribe", kafkaTopic)
      .option("kafka.bootstrap.servers", "master01.bigdata.alumnos.upcont.es:9092")
      .option("startingOffsets", "earliest")
      .load()
      .select(decode('value, "UTF-8") as 'line)
  }

  // TODO implement this method
  def buildStreamingPipeline(inputTable: DataFrame, stopWords: Seq[String]): DataFrame = {
    inputTable.withColumn("clean_line", regexp_replace('line, "[^A-Za-z0-9\\s]", ""))
        .withColumn("array_words", split('clean_line, "\\s+") as 'array_words) //regex101.com
        .withColumn("word", explode('array_words))
        .withColumn("lower_case", lower('word))
        //.filter('lower_case.isin(stopWords: _*))
        .where(!'lower_case.isin(stopWords: _*)) //

  }



  def main(args: Array[String]): Unit = {
    require(args.length == 2, "SplitAndCleanLines <output topic> <stopwords file path>")
    logger.info(s"Creating Spark Structured Streaming input table from topic $InputTopic")
    val it = getInputTableFromTopic(InputTopic)

    val stopWords = getFileLines(args(1))
    val resultTable = buildStreamingPipeline(it, stopWords)

    val topic = args(0)

    logger.info(s"Writing result to Kafka topic: $topic")

    val streamingQuery = resultTable
            .select('lower_case cast BinaryType as 'value)
            .writeStream
            .format("kafka")
            .option("topic", topic)
            .option("kafka.bootstrap.servers", "master01.bigdata.alumnos.upcont.es:9092")
            .option("checkpointLocation", s"/tmp/spark/streaming/checkpoint/$user/words")
            .trigger(Trigger.ProcessingTime("5 seconds"))
            .start()


    logger.info(s"Streaming query ${streamingQuery.name} started!")
    streamingQuery.awaitTermination()

  }
}