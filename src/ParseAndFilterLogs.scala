import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types.TimestampType

import scala.io.Source

/**
  * A Spark Structured Streaming application to parse and filter NginX access log lines
  */
object ParseAndFilterLogs {

  // TODO set your user name here
  val user: String = "aalvarez"

  val InputTopic = "streaming.logs.nginx"

  private val logger = Logger.getLogger(getClass.getName)

  private val spark = SparkSession.builder()
    .appName(s"ParseAndFilterLogs - $user")
    .master("yarn")
    .getOrCreate()

  // this is needed to use symbols as column names
  import spark.implicits._


  /**
    * Returns a streaming dataframe from the specified topic
    * @param kafkaTopic
    * @return
    */
  def getInputTableFromTopic(kafkaTopic: String): DataFrame = {
    spark.readStream
      .format("kafka")
      .option("subscribe", kafkaTopic)
      .option("kafka.bootstrap.servers", "master01.bigdata.alumnos.upcont.es:9092")
      .option("startingOffsets", "earliest")
      .load()
      .select(decode('value, "UTF-8") as 'line)
  }


  // TODO implement this method
  def buildStreamingPipeline(inputTable: DataFrame): DataFrame =
    inputTable.filter('line.contains("html"))//.filter('line.like(".*html.*"))
      .withColumn("origin_ip", regexp_extract('line, "(\\d{3}\\.\\d{3}\\.\\d{2}\\.\\d{2})", 1))
      .withColumn("URL", regexp_extract('line, "(/[A-Za-z0-9/]+\\.html)", 1))
      .withColumn("ts", regexp_extract('line, "(\\d{2}/\\w{3}/\\d{4}:\\d{2}:\\d{2}:\\d{2})", 1))
      .withColumn("ts", to_timestamp('ts, "dd/MMM/yyyy:HH:mm:ss")) //dd/MMM/yyyy:HH:mm:ss
      .withColumn("ts", date_format('ts, "yyyy-mm-dd HH:mm:ss"))
      .select('ts, 'origin_ip, 'URL)


  def main(args: Array[String]): Unit = {
    require(args.length == 1, "ParseAndFilterLogs <output HDFS file path>")
    logger.info(s"Creating Spark Structured Streaming input table from topic $InputTopic")
    val it = getInputTableFromTopic(InputTopic)

    val resultTable = buildStreamingPipeline(it)

    val path = args(0)

    logger.info(s"Writing result to parquet file: $path")

    val streamingQuery = resultTable
      .writeStream
      .format("parquet")
      .option("path", path)
      .option("checkpointLocation", s"/tmp/spark/streaming/checkpoint/$user/logs")
      .trigger(Trigger.ProcessingTime("5 seconds"))
      .start()


    logger.info(s"Streaming query ${streamingQuery.name} started!")
    streamingQuery.awaitTermination()

  }
}