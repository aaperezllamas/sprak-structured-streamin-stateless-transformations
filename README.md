# Practica 2 - Spark Structured Streaming - Stateless transformations

## Ejercicio SplitAndCleanLines

### Objetivo

El objetivo de este ejercicio es procesar cada linea del topic `streaming.books` y realizar las siguientes
transformaciones sin estado:

* Eliminar caracteres que no sean letras, números o el apóstrofe (')
* Separar cada linea en sus palabras
* Convertir a minuscula cada palabra
* Eliminar las palabras contenidas en un fichero de stopwords especificado como segundo argumento

El resultado se publicará en el topic especificado como primer argumento.

El pipeline de transformación se codificará empleando únicamente el API de Spark SQL (Dataframes + funciones sql)
La referencia de estas funciones se puede encontrar en este enlace de la documentación oficial:

https://spark.apache.org/docs/2.3.1/api/scala/index.html

### Compilación y empaquetado

Para compilar y empaquetar la aplicación, ejecutar desde este mismo directorio el siguiente comando:

```bash
mvn clean package
```

También se puede ejecutar la tarea Maven desde el IDE (vista Maven -> Lifecycle -> package)

### Ejecución

El proyecto está configurado para generar un _fat jar_ ejecutable bajo el directorio `target`. Para ejecutarlo, copiarlo 
al nodo edge01 y utilizar `spark2-submit` de la siguiente manera:

```bash
scp target/spark-structured-stateless-0.1.0-SNAPSHOT.jar edge:
spark2-submit --master yarn --num-executors 2 --executor-cores 2 --executor-memory 1G \
--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0  --class edu.comillas.mbd.streaming.spark.practices.SplitAndCleanLines \
spark-structured-stateless-0.1.0-SNAPSHOT.jar <input topic> <stop words file>
```

## Ejercicio ParseAndFilterLogs

### Objetivo

El objetivo de este ejercicio es procesar las lineas de log del topic `streaming.nginx.logs`, parsearlas, filtrarlas
y almacenarlas en una tabla parquet en HDFS.

Respecto al filtrado, solo debemos almacenar en la tabla de salida las URLs de recursos html (hay que filtrar los recursos de tipo imagen)

Como ejemplo del formato de una línea tenemos el siguiente:

```bash
192.168.80.32 - - [19/Mar/2019:19:12:32 +0000] "GET /org/apache/spark/util/AccumulatorV2.html HTTP/1.1" 200 40522 "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:65.0) Gecko/20100101 Firefox/65.0"
```

La tabla parquet resultante debe tener un esquema como el siguiente:

| ts (string)         | origin_ip (string) | URL                                       |
|---------------------|--------------------|-------------------------------------------|
| 2019-03-19 19:12:32 | 192.168.80.32      | /org/apache/spark/util/AccumulatorV2.html |

### Ejecución

El proyecto está configurado para generar un _fat jar_ ejecutable bajo el directorio `target`. Para ejecutarlo, copiarlo 
al nodo edge01 y utilizar `spark2-submit` de la siguiente manera:

```bash
scp target/spark-structured-stateless-0.1.0-SNAPSHOT.jar edge:
spark2-submit --master yarn --num-executors 2 --executor-cores 2 --executor-memory 1G \
--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0  --class edu.comillas.mbd.streaming.spark.practices.ParseAndFilterLogs \
spark-structured-stateless-0.1.0-SNAPSHOT.jar <parquet HDFS file path>
```
